#CC=musl-gcc
CC=gcc
#Debug build
CFLAGS=-D_DEFAULT_SOURCE -g -O0 -pedantic -std=c89 -Wall -Werror -Wextra
#Release build
#CFLAGS=-Wall -O2
C_FILES=$(subst src/,,$(wildcard src/*.c))
O_FILES=$(subst .c,.o,$(C_FILES))
OUTPUT=dcmp
FDUP_TOOL=fdup


$(FDUP_TOOL): dirdup.c strlcat.o strlcpy.o walk.o
	$(CC) $(CFLAGS) -I./src -pipe -o $@ $^


$(OUTPUT): $(O_FILES)
	$(CC) -static -o $@ $^


%.o: src/%.c
	$(CC) -c $(CFLAGS) -o $@ $<


clean:
	rm -f *.o $(OUTPUT) $(FDUP_TOOL)

