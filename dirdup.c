#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include <strlcpy.h>
#include <strlcat.h>
#include <walk.h>

static const char *source_path_;
static const char *target_path_;

static int
print_path(const struct cb_params *prm)
{
	char fp[1200];
	struct stat s;
	int n;
	*fp = '\0';
	if (prm->entry_type != DT_DIR) {
		strlcpy(fp, target_path_, sizeof(fp));
		if (*prm->current_dir != '/') add_slash(fp, sizeof(fp));
		strlcat(fp, prm->current_dir, sizeof(fp));
		add_slash(fp, sizeof(fp));
		strlcat(fp, prm->entry_path, sizeof(fp));
		n = stat(fp, &s);
		if (n != -1)
			printf("Found\n%s\n%s/%s/%s\n", fp, source_path_,
					prm->current_dir, prm->entry_path);
	}
	return WALK_CONTINUE;
}

int
main(int argc, char **argv)
{
	struct walk_entry w;
	if (argc < 3) {
		fprintf(stderr, "Usage: %s <source catalog> <dest catalog>\n",
				argv[0]);
		return 1;
	}
	w.param = NULL;
	w.cb = print_path;
	source_path_ = argv[1];
	target_path_ = argv[2];
	dir_walk_nr(argv[1], 40, &w);
	return 0;
}
