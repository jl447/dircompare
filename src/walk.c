#include <errno.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "strlcat.h"
#include "strlcpy.h"
#include "walk.h"

#define IS_DOT_OR_DOTDOT(x) ((*(unsigned short *)x == 0x2e) || \
	((*(unsigned int *)x & 0xffffff) == 0x2e2e))

#ifndef PATH_MAX
#define PATH_MAX 4096
#endif

void
add_slash(char *ptr, unsigned long buflen)
{
	size_t n = strlen(ptr);
	if (ptr[n - 1] == '/' || n + sizeof(unsigned short) > buflen) return;
	*(unsigned short *)(ptr + n) = 0x2f;
}

int
dir_walk_nr(const char *dirpath, int max_depth, struct walk_entry *wp)
{
	DIR **catalogs = (DIR **)calloc(max_depth > 0 ?
			max_depth : DEFAULT_MAX_DEPTH, sizeof(DIR *));
	int level;
	char *path = NULL;
	size_t n_head;
	struct dirent *ent = NULL;
	char *slashpos = NULL;
	enum walk_cb_status flag;
	struct cb_params callback_param;
	int r = 0;
	if (catalogs == NULL)
		return errno;
	if (max_depth <= 0) max_depth = DEFAULT_MAX_DEPTH;
	catalogs[0] = opendir(dirpath);
	if (catalogs[0] == NULL) {
		fprintf(stderr, "Error: cannot open %s: %s\n",
				dirpath, strerror(errno));
		r = errno;
		goto err_catalogs;
	}
	level = 0;
	path = (char *)malloc(PATH_MAX);
	if (path == NULL) {
		r = errno;
		goto err_catalogs;
	}
	strlcpy(path, dirpath, PATH_MAX);
	n_head = strlen(path);
	callback_param.param = wp->param;
	while (1) {
		ent = readdir(catalogs[level]);
		if (ent == NULL) {
			closedir(catalogs[level]);
			-- level;
			if (level == -1) break;
			slashpos = strrchr(path + n_head, '/');
			if (slashpos) {
				*slashpos = '\0';
			} else {
				path[n_head] = '\0';
			}
			continue;
		}
		if (IS_DOT_OR_DOTDOT(ent->d_name)) continue;
		callback_param.current_dir = path + n_head;
		callback_param.entry_path = ent->d_name;
		callback_param.entry_type = ent->d_type;
		flag = wp->cb(&callback_param);
		if (flag == WALK_STOP) {
			for (;level != -1; -- level) {
				closedir(catalogs[level]);
			}
			break;
		}
		if (ent->d_type == DT_DIR && flag != WALK_SKIP_DIR) {
			if (level + 1 >= max_depth) {
				fprintf(stderr, "Error: path \"%s\" too deep\n",
						path);
				continue;
			}
			add_slash(path, PATH_MAX);
			strlcat(path, ent->d_name, PATH_MAX);
			catalogs[++ level] = opendir(path);
			if (catalogs[level] == NULL) {
				fprintf(stderr, "Cannot open %s: %s\n",
						path, strerror(errno));
				-- level;
				*strrchr(path + n_head, '/') = '\0';
				continue;
			}
		}
	}
	free(path);
err_catalogs:
	free(catalogs);
	return r;
}
