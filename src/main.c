#include <dirent.h> /* Detect d_type. */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "strlcat.h"
#include "strlcpy.h"
#include "walk.h"

static int
check_path(const struct cb_params *p)
{
	char buf[512] = {0};
	struct stat l;
	int n;
	strlcpy(buf, (const char *)p->param, sizeof(buf));
	if (p->current_dir[0] != '/') add_slash(buf, sizeof(buf));
	strlcat(buf, p->current_dir, sizeof(buf));
	add_slash(buf, sizeof(buf));
	strlcat(buf, p->entry_path, sizeof(buf));
	n = stat(buf, &l);
	if (p->entry_type == DT_DIR) {
		if (n == -1 || !S_ISDIR(l.st_mode)) {
			printf("No \"%s\" directory\n", buf);
			return WALK_SKIP_DIR;
		}
	}
	if (n == -1) {
		if (errno == ENOENT) {
			printf("Path \"%s\" is not present\n", buf);
		} else {
			fprintf(stderr, "Cannot stat %s: %s\n", buf,
					strerror(errno));
		}
	}
	return WALK_CONTINUE;
}

int
main(int argc, char **argv)
{
	int i;
	struct stat l;
	struct walk_entry w;
	if (argc < 3) {
		fprintf(stderr, "Usage: %s <source dir> <target dir> "
				"[<max depth>, min 4]\n",
				argv[0]);
		return 0;
	}
	for (i = 1; i < 3; ++ i) {
		if (stat(argv[i], &l) == -1) {
			fprintf(stderr, "Cannot stat %s: %s\n", argv[i],
					strerror(errno));
			return 1;
		}
		if (!S_ISDIR(l.st_mode)) {
			fprintf(stderr, "Error: %s is not a directory\n",
					argv[i]);
			return 1;
		}
	}
	w.cb = check_path;
	w.param = argv[2];
	i = 40;
	if (argc > 3 && (i = atoi(argv[3])) < 4)
		i = 40;
	return dir_walk_nr(argv[1], i, &w);
}
