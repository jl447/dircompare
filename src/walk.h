#define DEFAULT_MAX_DEPTH 20

/*
 First parameter is a subdir in base path, and second is current dirent->d_name.
*/

struct cb_params {
  const char *current_dir;
  const char *entry_path;
  unsigned char entry_type;
  void *param;
};

typedef int (*path_callback)(const struct cb_params *cb_info);

struct walk_entry {
  path_callback cb;
  void *param;
};

enum walk_cb_status {
  WALK_CONTINUE,
  WALK_SKIP_DIR,
  WALK_STOP
};

void
add_slash(char *ptr, unsigned long buflen);

/*
 * dir_walk_nr - walk dirpath in non-recursive manner.
 * dirpath is target dir;
 * max_depth is maximum path depth level, defaults to DEFAULT_MAX_DEPTH,
 * if max_depth <= 0;
 * filter is function called for each path component found.
 * */
int
dir_walk_nr(const char *dirpath, int max_depth, struct walk_entry *wp);
